\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction} %% For this chapter to appear in toc

The general framework in which this dissertation takes place is the
\emph{homotopy theory of strict $\oo$\nbd{}categories}, and, as the title
suggests, its focus is on homological aspects of this theory. The goal is to
study and compare two different homological invariants for strict
$\oo$\nbd{}categories; that is to say, two different
functors \[\mathbf{Str}\oo\Cat \to \ho(\Ch)\] from the category of strict
$\oo$\nbd{}categories to the homotopy category of non-negatively graded chain
complexes (i.e.\ the localization of the category of non-negatively graded chain
complexes with respect to the quasi\nbd{}isomorphisms).

Before we enter into the heart of the subject, let us emphasize that, with the
sole exception of the end of this introduction, all the $\oo$\nbd{}categories
that we consider are strict $\oo$\nbd{}categories. Hence, we drop the adjective
``strict'' and simply say \emph{$\oo$\nbd{}category} instead of \emph{strict
  $\oo$\nbd{}category} and we write $\oo\Cat$ instead of $\mathbf{Str}\oo\Cat$
for the category of (strict) $\oo$\nbd{}categories.


\begin{named}[Background: $\oo$-categories as spaces] The homotopy theory of
  $\oo$\nbd{}categories begins with the nerve functor introduced by Street in
  \cite{street1987algebra}
  \[
    N_{\omega} : \oo\Cat \to \Psh{\Delta}
  \]
  that associates to every $\oo$\nbd{}category $C$ a simplicial set $N_{\oo}(C)$
  called the \emph{nerve of $C$}, generalizing the usual nerve of (small)
  categories. Using this functor, we can transfer the homotopy theory of
  simplicial sets to $\oo$\nbd{}categories, as it is done for example in the
  articles
  \cite{ara2014vers,ara2018theoreme,gagna2018strict,ara2019quillen,ara2020theoreme,ara2020comparaison}.
  Following the terminology of these articles, a morphism $f : C \to D$ of
  $\oo\Cat$ is a \emph{Thomason equivalence} if $N_{\omega}(f)$ is a Kan--Quillen
  weak equivalence of simplicial sets. By definition, the nerve functor induces
  a functor at the level of homotopy categories
  \[
    \overline{N_{\omega}} : \ho(\oo\Cat^{\Th}) \to \ho(\Psh{\Delta}),
  \]
  where $\ho(\oo\Cat^{\Th})$ is the localization of $\oo\Cat$ with respect to
  the Thomason equivalences and $\ho(\Psh{\Delta})$ is the localization of
  $\Psh{\Delta}$ with respect to the Kan--Quillen weak equivalences of simplicial
  sets. As it so happens, the functor $\overline{N_{\omega}}$ is an equivalence
  of categories, as proved by Gagna in \cite{gagna2018strict}. In other words,
  the homotopy theory of $\oo$\nbd{}categories induced by Thomason equivalences
  is the same as the homotopy theory of spaces. Gagna's result is in fact a
  generalization of the analogous result for the usual nerve of small
  categories, which is attributed to Quillen in \cite{illusie1972complexe}. In
  the case of small categories, Thomason even showed the existence of a model
  structure whose weak equivalences are the ones induced by the nerve functor
  \cite{thomason1980cat}. The analogous result for $\oo\Cat$ is conjectured but
  not yet established \cite{ara2014vers}.
\end{named}
\begin{named}[Two homologies for $\oo$-categories]
  Keeping in mind the nerve functor of Street, a natural thing to do is to
  define the \emph{$k$-th homology group of an $\oo$\nbd{}category $C$} as the
  $k$\nbd{}th homology group of the nerve of $C$. In light of Gagna's result, these
  homology groups are just another way of looking at the homology groups of
  spaces. In order to explicitly avoid future confusion, we shall now use the
  name \emph{singular homology groups} of $C$ for these homology groups and the
  notation $H^{\sing}_k(C)$.

  On the other hand, Métayer gives a definition in \cite{metayer2003resolutions}
  of other homology groups for $\oo$\nbd{}categories. This definition is based
  on the notion of \emph{$\oo$\nbd{}categories free on a polygraph} (also known
  as \emph{$\oo$\nbd{}categories free on a computad}), which are
  $\oo$\nbd{}categories that are obtained from the empty category by recursively
  freely adjoining cells. From now on, we simply say \emph{free
    $\oo$\nbd{}category}. Métayer observed that every $\oo$\nbd{}category $C$ admits
  what we call a \emph{polygraphic resolution}, which means that there exists a
  free $\oo$\nbd{}category $P$ and a morphism of $\oo\Cat$
  \[
    f : P \to C
  \]
  that satisfies properties formally resembling those of trivial fibrations of
  topological spaces (or of simplicial sets). Furthermore, every free
  $\oo$\nbd{}category $P$ can be ``abelianized'' to a chain complex $\lambda(P)$ and
  Métayer proved that for two different polygraphic resolutions $P \to C$ and
  $P' \to C$ of the same
  $\oo$\nbd{}category $C$, the chain complexes
  $\lambda(P)$ and $\lambda(P')$ are quasi-isomorphic. Hence, we can define the
  \emph{$k$-th polygraphic homology group} of $C$, denoted by $H_k^{\pol}(C)$,
  as the $k$-th homology group of $\lambda(P)$ for any polygraphic resolution $P
  \to C$.

  One is then led to the following question:
  \begin{center}
    Do we have $H_{\bullet}^{\pol}(C) \simeq H_{\bullet}^{\sing}(C)$ for every
    $\oo$\nbd{}category $C$?
  \end{center}
  \iffalse
  \begin{equation}\label{naivequestion}\tag{\textbf{Q}}
    \text{Do we have }H_k^{\pol}(C) \simeq H_k^{\sing}(C)\text{ for every }\oo\text{-category }C\text{ ? }
  \end{equation}
  \fi A first partial answer to this question is given by Lafont and Métayer in
  \cite{lafont2009polygraphic}: for a monoid $M$ (seen as category with one
  object and hence as
  an $\oo$\nbd{}category), we have $H_{\bullet}^{\pol}(M) \simeq
  H_{\bullet}^{\sing}(M)$. In fact, the original motivation for polygraphic
  homology was the homology of monoids and is part of a program that generalizes
  to higher dimension the results of Squier on the rewriting theory of monoids
  \cite{guiraud2006termination,lafont2007algebra,guiraud2009higher,guiraud2018polygraphs}. However, interestingly
  enough, the general answer to the above question is \emph{no}. A
  counterexample was found by Maltsiniotis and Ara. Let $B$ be the commutative
  monoid $(\mathbb{N},+)$, seen as a $2$\nbd{}category with only one $0$\nbd{}cell and no
  non-trivial $1$-cells. This $2$\nbd{}category is free (as an $\oo$\nbd{}category)
  and a quick computation shows that:
  \[
    H_k^{\pol}(B)=\begin{cases} \mathbb{Z} &\text{ if } k=0,2 \\ 0 &\text{
        otherwise. }\end{cases}
  \]
  On the other hand, it is shown in \cite[Theorem 4.9 and Example
  4.10]{ara2019quillen} that the nerve of $B$ is a $K(\mathbb{Z},2)$; hence, it
  has non-trivial homology groups in all even dimension.

  A question that still remains is:
  \begin{center}
    \textbf{(Q)} Which are the $\oo$\nbd{}categories $C$ such that
    $H_{\bullet}^{\pol}(C) \simeq H_{\bullet}^{\sing}(C)$ ?
  \end{center}
  This is precisely the question around which this dissertation revolves.
  Nevertheless, the reader will also find several new notions and results within
  this document that, although primarily motivated by the above question, are of
  interest in the theory of $\oo$\nbd{}categories and whose \emph{raisons
    d'être} go beyond the above considerations.
\end{named}
\begin{named}[Another formulation of the problem] One of the achievements of the
  present work is a more abstract reformulation of the question of comparison of
  singular and polygraphic homology of
  $\oo$\nbd{}categories. %As often, the reward for abstraction is a much clearer understanding of the problem.

  In order to do so, recall first that by a variation of the Dold--Kan
  equivalence (see for example \cite{bourn1990another}), the category of abelian
  group objects in $\oo\Cat$ is equivalent to the category of non-negatively
  graded chain complexes
  \[
    \Ab(\oo\Cat) \simeq \Ch.
  \]
  Hence, we have a forgetful functor $\Ch \simeq \Ab(\oo\Cat) \to \oo\Cat$,
  which has a left adjoint
  \[
    \lambda : \oo\Cat \to \Ch.
  \]
  Moreover, for a \emph{free} $\oo$\nbd{}category $C$, the chain complex
  $\lambda(C)$ is exactly the one obtained by the ``abelianization'' process
  considered in Métayer's definition of polygraphic homology.

  Now, the category $\oo\Cat$ admits a model structure, known as the \emph{folk
    model structure} \cite{lafont2010folk}, whose weak equivalences are the
  \emph{equivalences of $\oo$\nbd{}categories} (a generalization of the usual
  notion of equivalence of categories) and whose cofibrant objects are exactly
  the free $\oo$\nbd{}categories \cite{metayer2008cofibrant}. Polygraphic
  resolutions are then nothing but cofibrant replacements in this model
  category. As the definition of polygraphic homology groups strongly suggests,
  the functor $\lambda$ is left Quillen with respect to this model structure. In
  particular, it admits a left derived functor
  \[
    \LL \lambda^{\folk} : \ho(\oo\Cat^{\folk}) \to \ho(\Ch)
  \]
  and we tautologically have that $H_k^{\pol}(C) = H_k(\LL \lambda^{\folk}(C))$
  for every $\oo$\nbd{}category $C$ and every $k \geq 0$. From now on, we set
  \[
    \sH^{\pol}(C):=\LL \lambda^{\folk}(C).
  \]
  This way of understanding polygraphic homology as a left derived functor has
  been around in the folklore for some time and I claim absolutely no
  originality for
  it. %Notice by the way, that the polygraphic homology of an $\oo$\nbd{}category is now an object of $\ho(\Ch)$ and not only a mere sequence of abelian groups.

  On the other hand, $\lambda$ is also left derivable when $\oo\Cat$ is equipped
  with Thomason equivalences, yielding a left derived functor
  \[
    \LL \lambda^{\Th} : \ho(\oo\Cat^{\Th}) \to \ho(\Ch).
  \]
  This left derived functor being such that $ H_k^{\sing}(C) = H_k(\LL
  \lambda^{\Th}(C))$ for every $\oo$\nbd{}category $C$ and every $k \geq 0$.
  Contrary to the ``folk'' case, this result is new and first appears within
  this document (at least to my knowledge). Note that since, as mentioned
  earlier, the existence of a Thomason-like model structure on $\oo\Cat$ is
  still conjectural, usual tools from Quillen's theory of model categories were
  unavailable to prove the left derivability of $\lambda$ and the difficulty was
  to find a workaround solution.

  From now on, we set
  \[
    \sH^{\sing}(C):=\LL \lambda^{\Th}(C).
  \]

  Finally, it can be shown that every equivalence of $\oo$\nbd{}categories is a
  Thomason equivalence. Hence, the identity functor of $\oo\Cat$ induces a
  functor $\J$ at the level of homotopy categories
  \[
    \J : \ho(\oo\Cat^{\folk}) \to \ho(\oo\Cat^{\Th}),
  \]
  and altogether we have a triangle
  \[
    \begin{tikzcd}
      \ho(\oo\Cat^{\folk}) \ar[rd,"\LL \lambda^{\folk}"'] \ar[r,"\J"] & \ho(\oo\Cat^{\Th}) \ar[d,"\LL \lambda^{\Th}"] \\
      & \ho(\Ch).
    \end{tikzcd}
  \]
  This triangle is \emph{not} commutative (even up to isomorphism), since this
  would imply that the singular and polygraphic homology groups coincide for every
  $\oo$\nbd{}category. However, since both functors $\LL \lambda^{\folk}$ and
  $\LL \lambda^{\Th}$ are left derived functors of the same functor $\lambda$,
  the existence of a natural transformation $\pi : \LL \lambda^{\Th} \circ \J
  \Rightarrow \LL \lambda^{\folk}$ follows by universal property. Since $\J$ is
  the identity on objects, for every $\oo$\nbd{}category $C$, this natural
  transformation yields a map
  \[
    \pi_C : \sH^{\sing}(C) \to \sH^{\pol}(C),
  \]
  which we refer to as the \emph{canonical comparison map}. Let us say that $C$
  is \emph{homologically coherent} if $\pi_C$ is an isomorphism (which means
  exactly that for every $k\geq 0$, the induced map $H^{\sing}_k(C) \to
  H_k^{\pol}(C)$ is an isomorphism). The question of study then becomes:
  \begin{center}
    \textbf{(Q')} Which $\oo$\nbd{}categories are homologically coherent ?
  \end{center}
  Note that, in theory, question \textbf{(Q')} is more precise than question
  \textbf{(Q)} since we impose which morphism has to be an isomorphism in the
  comparison of homology groups. However, for all the concrete examples that we shall
  meet in practice, it is always question \textbf{(Q')} that will be answered.
  % in practice, when we show that the
  % polygraphic and singular homology groups of an $\oo$\nbd{}category are
  % isomorphic, it is always via the above canonical comparison map. Conversely,
  % when we show that an $\oo$\nbd{}category is not \good{}, it always rules out
  % any isomorphism possible (not only the canonical comparison map).

  As will be explained in this thesis, a formal consequence of the above is that
  polygraphic homology is \emph{not} invariant under Thomason equivalence. This
  means that there exists at least one Thomason equivalence $f : C \to D$ such
  that the induced map
  \[
    \sH^{\pol}(C) \to \sH^{\pol}(D)
  \]
  is \emph{not} an isomorphism. % Indeed, if this was not the case, then $\LL
  % \lambda^{\folk}$ would factor through $\J$, yielding a functor
  % ${\ho(\oo\Cat^{\Th}) \to \ho(\Ch)}$, which can easily be proved by universal
  % property to be (canonically isomorphic to) $\LL \lambda^{\Th}$.
  % In particular, this would imply that every $\oo$\nbd{}category is
  % homologically coherent, which, as we have already seen, is not true.
  In other words, if we think of $\oo\Cat$ as a model of homotopy types (via the
  localization by the Thomason equivalences), then the polygraphic homology is
  \emph{not} a well-defined invariant. Another point of view would be to
  consider that the polygraphic homology is an intrinsic invariant of
  $\oo$\nbd{}categories (and not up to Thomason equivalence) and in that way is
  finer than singular homology. This is not the point of view adopted here, and
  the reason will be motivated at the end of this introduction. The slogan to
  retain is:
  \begin{center}
    Polygraphic homology is a way of computing singular homology groups of a
    homologically coherent $\oo$\nbd{}category.
  \end{center}
  The point is that given a \emph{free} $\oo$\nbd{}category $P$ (which is thus
  its own polygraphic resolution), the chain complex $\lambda(P)$ is much
  ``smaller'' than the chain complex associated to the nerve of $P$ and hence
  the polygraphic homology groups of $P$ are much easier to compute than its
  singular homology groups. The situation is comparable to using cellular
  homology for computing singular homology of a CW-complex. The difference is
  that in this last case, such a thing is always possible while in the case of
  $\oo$\nbd{}categories, one must ensure that the (free) $\oo$\nbd{}category is
  homologically
  coherent. %Intuitively speaking, this means that some free $\oo$\nbd{}categories are not ``cofibrant enough'' for homology.
\end{named}
\begin{named}[Finding homologically coherent $\oo$-categories]
  One of the main results presented in this dissertation is:
  \begin{center}
    Every (small) category $C$ is homologically coherent.
  \end{center}
  In order for this result to make sense, one has to consider categories as
  $\oo$\nbd{}categories with only unit cells above dimension $1$. Beware that
  this does not make the result trivial because given a polygraphic resolution
  $P \to C$ of a small category $C$, the $\oo$\nbd{}category $P$ need \emph{not}
  have only unit cells above dimension $1$.
  
  As such, this result is only a small generalization of Lafont and Métayer's
  result concerning monoids (although this new result, even restricted to
  monoids, is more precise because it means that the \emph{canonical comparison
    map} is an isomorphism). But the true novelty lies in the proof which is
  more conceptual that the one of Lafont and Métayer. It requires the
  development of several new concepts and results which in the end combine
  together smoothly to yield the desired result. This dissertation has been
  written so that all the elements needed to prove this result are spread over
  several chapters; a more condensed version of it is the object of the article
  \cite{guetta2020homology}. Among the new notions developed along the way, that
  of discrete Conduché $\oo$\nbd{}functor is probably the most significant. An
  $\oo$\nbd{}functor $f : C \to D$ is a \emph{discrete Conduché
    $\oo$\nbd{}functor} when for every cell $x$ of $C$, if $f(x)$ can be written
  as
  \[
    f(x)=y'\comp_k y'',
  \]
  then there exists a unique pair $(x',x'')$ of cells of $C$ that are
  $k$\nbd{}composable and such that
  \[
    f(x')=y',\, f(x'')=y'' \text{ and } x=x'\comp_k x''.
  \]
  The main result that we prove concerning discrete Conduché $\oo$\nbd{}functors
  is that for a discrete Conduché $\oo$\nbd{}functor $f : C \to D$, if the
  $\oo$\nbd{}category $D$ is free, then $C$ is also free. The proof of this
  result is long and tedious, though conceptually not extremely hard, and first
  appears in the paper \cite{guetta2020polygraphs}, which is dedicated to it.

  After having settled the case of ($1$\nbd{})categories, it is natural to move
  on to $2$\nbd{}categories. Contrary to the case of ($1$\nbd{})categories, not
  all $2$\nbd{}categories are \good{} and the situation seems to be much harder
  to understand. As a simplification, one can focus on $2$\nbd{}categories which
  are free (as $\oo$\nbd{}categories). This is what is done in this
  dissertation. With this extra hypothesis, the problem of characterization of
  \good{} free $2$\nbd{}categories may be reduced to the following question:
  given a cocartesian square of the form
  \[
    \begin{tikzcd}
      \sS_1 \ar[r] \ar[d] & P \ar[d]\\
      \sD_2 \ar[r] & P', \ar[from=1-1,to=2-2,"\ulcorner",phantom,very near end]
    \end{tikzcd}
  \]
  where $P$ is a free $2$\nbd{}category, when is it \emph{homotopy cocartesian}
  with respect to the Thomason equivalences? As a consequence, a substantial part of
  the work presented here consists in developing tools to detect homotopy
  cocartesian squares of $2$\nbd{}categories with respect to the Thomason
  equivalences. While it appears that these tools do not allow to completely
  answer the above question, they still make it possible to detect such homotopy
  cocartesian squares in many concrete situations. In fact, a whole section of
  the thesis is dedicated to giving examples of (free) $2$\nbd{}categories and
  computing the homotopy type of their nerve using these tools. Among all these
  examples, a particular class of well-behaved $2$\nbd{}categories, which I have
  coined ``bubble-free $2$\nbd{}categories'', seems to stand out. This class is
  easily characterized as follows. Given a $2$\nbd{}category, let us call
  \emph{bubble} a non-trivial $2$\nbd{}cell whose source and target are units on
  a $0$\nbd{}cell (necessarily the same). A \emph{bubble-free $2$\nbd{}category}
  is then nothing but a $2$\nbd{}category that has no bubbles. The archetypal
  example of a $2$\nbd{}category that is \emph{not} bubble-free is the
  $2$\nbd{}category $B$ introduced earlier (which is the commutative monoid
  $(\mathbb{N},+)$ seen as a $2$\nbd{}category). As already said, this
  $2$\nbd{}category is not \good{} and this does not seem to be a coincidence.
  It is indeed remarkable that of all the many examples of $2$\nbd{}categories
  studied in this work, the only ones that are not \good{} are exactly the ones
  that are \emph{not} bubble-free. This leads to the conjecture below, which
  stands as a conclusion of the thesis.
  \begin{center}
    \textbf{(Conjecture)} A free $2$\nbd{}category is \good{} if and only if it
    is bubble-free.
  \end{center}
\end{named}
\begin{named}[The big picture]
  Let us end this introduction with another point of view on the comparison of
  singular and polygraphic homologies. This point of view is highly conjectural
  and is not addressed at all in the rest of the dissertation. It should be
  thought of as a guideline for future work.

  In the same way that (strict) $2$\nbd{}categories are particular cases of
  bicategories, strict $\oo$\nbd{}categories are in fact particular cases of
  what are usually called \emph{weak $\oo$\nbd{}categories}. Such mathematical
  objects have been defined, for example, by Batanin using globular operads
  \cite{batanin1998monoidal} or by Maltsiniotis following ideas of Grothendieck
  \cite{maltsiniotis2010grothendieck}. Similarly to the fact that the theory of
  quasi-categories (which is a homotopical model for the theory of weak
  $\oo$\nbd{}categories whose cells are invertible above dimension $1$) may be
  expressed using the same language as the theory of usual categories, it is
  generally believed that all ``intrinsic'' notions (in a precise sense to be
  defined) of the theory of strict $\oo$\nbd{}categories have weak counterparts.
  For example, it is believed that there should be a folk model structure on the
  category of weak $\oo$\nbd{}categories and that there should be a good notion
  of free weak $\oo$\nbd{}category. In fact, this last notion should be defined
  as weak $\oo$\nbd{}categories that are recursively obtained from the empty
  $\oo$\nbd{}category by freely
  adjoining cells, which is the formal analogue of the strict version but in the
  weak context. The important point here is that a free strict
  $\oo$\nbd{}category is \emph{never} free as a weak $\oo$\nbd{}category (except
  for the empty $\oo$\nbd{}category).
  % For
  % example, the $2$\nbd{}category $B$ we have introduced earlier, which is free
  % as a strict
  % $\oo$\nbd{}category, seems to be \emph{not} free as
  % a weak $\oo$\nbd{}category.
  Moreover, there are good candidates for the polygraphic homology of weak
  $\oo$\nbd{}categories obtained by mimicking the definition in the strict case.
  But in general the polygraphic homology of a strict $\oo$\nbd{}category need
  not be the same as its ``weak polygraphic homology''. Indeed, since free
  strict $\oo$\nbd{}categories are not free as weak $\oo$\nbd{}categories,
  taking a ``weak polygraphic resolution'' of a strict $\oo$\nbd{}category is
  not the same as taking a polygraphic resolution. In fact, when trying to
  compute the weak polygraphic homology of $B$, it would seem that it gives the
  homology groups of a $K(\mathbb{Z},2)$, which is what we would have expected
  of its polygraphic homology in the first place. From this observation, it is
  tempting to make the following conjecture:
  \begin{center}
    The weak polygraphic homology of a strict $\oo$\nbd{}category coincides
    with its singular homology.
  \end{center}
  In other words, we conjecture that the fact that polygraphic and singular
  homologies of strict $\oo$\nbd{}categories do not coincide is a defect due to
  working in too narrow a setting. The ``good'' definition of polygraphic
  homology ought to be the weak one.

  
  We can go even further and conjecture the same thing for weak
  $\oo$\nbd{}categories. In order to do so, we need a definition of
  singular homology for weak $\oo$\nbd{}categories. This is
  conjecturally done as follows. To every weak $\oo$\nbd{}category
  $C$, one can associate a weak $\oo$\nbd{}groupoid $L(C)$ by formally
  inverting all the cells of $C$. Then, if we believe in
  Grothendieck's conjecture (see \cite{grothendieck1983pursuing} and
  \cite[Section 2]{maltsiniotis2010grothendieck}), the category of
  weak $\oo$\nbd{}groupoids equipped with the weak equivalences of weak
  $\oo$\nbd{}groupoids (see
  \cite[Paragraph 2.2]{maltsiniotis2010grothendieck}) is a model for the homotopy
  theory of spaces. In particular, every weak $\oo$\nbd{}groupoid has
  homology groups and we can define the singular homology groups of a
  weak $\oo$\nbd{}category $C$ as the homology groups of $L(C)$.

  %% This defines a functor
  %% \[
  %%   L : \mathbf{W}\oo\Cat \to \mathbf{W}\oo\Grpd
  %% \]
\end{named}
\begin{named}[Organization of the thesis]
  In the first chapter, we review some aspects of the theory of
  $\oo$\nbd{}categories. In particular, we study with great care free
  $\oo$\nbd{}categories, which are at the heart of the present work. It is the
  only chapter of the thesis that does not contain any reference to homotopy
  theory whatsoever. It is also there that we introduce the notion of discrete
  Conduché $\oo$\nbd{}functor and study their relation with free
  $\oo$\nbd{}categories. The culminating point of the chapter is Theorem
  \ref{thm:conduche}, which states that given a discrete Conduché
  $\oo$\nbd{}functor $F : C \to D$, if $D$ is free, then so is $C$. The proof of
  this theorem is long and technical and is broke down into several distinct
  parts.

  The second chapter is devoted to recalling some tools of homotopical algebra.
  More precisely, basic aspects of the theory of homotopy colimits using the
  formalism of Grothendieck's derivators are quickly presented. Note that this
  chapter does \emph{not} contain any original result and can be skipped at
  first reading. It is only intended to give the reader a summary of useful
  results on homotopy colimits that are used in the rest of the dissertation.

  In the third chapter, we delve into the homotopy theory of
  $\oo$\nbd{}categories. It is there that we define the different notions of
  weak equivalences for $\oo$\nbd{}categories and compare them. The two most
  significant new results to be found in this chapter are probably Proposition
  \ref{prop:folkisthom}, which states that every equivalence of
  $\oo$\nbd{}categories is a Thomason equivalence, and Theorem
  \ref{thm:folkthmA}, which states that equivalences of $\oo$\nbd{}categories
  satisfy a property reminiscent of Quillen's Theorem $A$ \cite[Theorem
  A]{quillen1973higher} and its $\oo$\nbd{}categorical generalization by Ara and
  Maltsiniotis \cite{ara2018theoreme,ara2020theoreme}.

  In the fourth chapter, we define the polygraphic and singular homologies of
  $\oo$\nbd{}categories and properly formulate the problem of their comparison.
  Up to Section \ref{section:polygraphichmlgy} included, all the results were
  known prior to this thesis (at least in the folklore), but starting from
  Section \ref{section:singhmlgyderived} all the results are original. Three
  fundamental results of this chapter are: Theorem \ref{thm:hmlgyderived},
  which states that singular homology is obtained as a derived functor of an
  abelianization function, Proposition \ref{prop:criteriongoodcat}, which gives
  an abstract criterion to detect \good{} $\oo$\nbd{}categories, and Proposition
  \ref{prop:comphmlgylowdimension}, which states that low-dimensional singular
  and polygraphic homology groups always coincide.

  The fifth chapter is mainly geared towards the fundamental Theorem
  \ref{thm:categoriesaregood}, which states that every category is \good{}. To
  prove this theorem, we first focus on a particular class of
  $\oo$\nbd{}categories, which we call \emph{contractible
    $\oo$\nbd{}categories}, and show that every contractible
  $\oo$\nbd{}category is \good{} (Proposition \ref{prop:contractibleisgood}).

  Finally, the sixth and last chapter of the thesis revolves around the homology
  of free $2$\nbd{}categories. The goal pursued is to try to understand which
  free $2$\nbd{}categories are \good{}. In order to do so, we give a criterion
  to detect homotopy cocartesian square with respect to Thomason equivalences
  (Proposition \ref{prop:critverthorThomhmtpysquare}) based on the homotopy
  theory of bisimplicial sets. Then, we apply this criterion and some other \emph{ad
  hoc} techniques to compute many examples of homotopy type of free
  $2$\nbd{}categories. The conclusion of the chapter is Conjecture
  \ref{conjecture:bubblefree}, which states that a free $2$\nbd{}category is
  \good{} if and only if it is bubble-free.
\end{named}
%% Let us come back to the canonical $2$-triangle
%% \[
%%   \begin{tikzcd}
%%     \ho(\oo\Cat^{\folk}) \ar[rd,"\sH^{\folk}=\LL \lambda^{\folk}"',""{name=B,above}] \ar[r,"\J"] & \ho(\oo\Cat^{\Th}) \ar[d,"\LL \lambda^{\Th}=\sH^{\sing}"] \\
%%     & \ho(\Ch) \ar[from=1-2,to=B,Rightarrow,"\pi"]
%%   \end{tikzcd}
%% \]
%% and ask the question:
%% \begin{center}
%%   What \emph{would} it mean that the natural transformation $\pi$ be an
%%   isomorphism (i.e.\ that all $\oo$\nbd{}categories be homologically
%%   coherent) ?
%% \end{center}
%% For simplification, let us assume that the conjectured Thomason-like model
%% structure on $\oo\Cat$ was established and that $\lambda$ was left Quillen
%% with respect to this model structure (which is also conjectured). Now, the
%% conjectured cofibrations of the Thomason-like model structure (see
%% \cite{ara2014vers}) are particular cases of folk cofibrations and thus, all
%% Thomason cofibrant objects are folk cofibrant objects. The converse, on the
%% other hand, is not true. Consequently, Quillen's theory of derived functors
%% tells us that for a \emph{Thomason} cofibrant object $P$, we have
%% \begin{equation}\tag{$\ast$}\label{equationintro}
%%   \LL \lambda^{\Th}(P) \simeq \lambda(P) \simeq \LL \lambda^{\folk}(P),
%%   \end{equation}
%%   (and the resulting isomorphism is obviously the canonical comparison map).
%%   Now, \emph{if} the natural transformation $\pi$ were an isomorphism, then a
%%   quick 2-out-of-3 reasoning would show that \eqref{equationintro} would also
%%   be true when $P$ is only \emph{folk} cofibrant. Hence, intuitively
%%   speaking, if $\pi$ were an isomorphism, then folk cofibrant objects would
%%   be \emph{sufficiently cofibrant} for the homology, even though there are
%%   not Thomason cofibrant. (And in fact, using cofibrant replacements, it can
%%   be shown that this condition is sufficient to ensure that $\pi$ be an
%%   isomorphism).

%%   Yet, as we have already seen, such property is not true: there are folk
%%   cofibrant objects that are \emph{not} enough cofibrant to compute (Street)
%%   homology. The archetypal example being the ``bubble'' of Ara and
%%   Maltsiniotis. However, even if false, the idea that folk cofibrant objects
%%   are sufficiently cofibrants for homology is seducing and I conjecturally
%%   believe that this defect is a mere consequence of working in a too narrow
%%   setting, as I shall now explain.

%%   In the same way that bicategories and tricategories are ``weak'' variations
%%   of the notions of (strict) $2$-categories and $3$-categories, there exists
%%   a general notion of \emph{weak $\oo$\nbd{}categories}. These objects can be
%%   defined, for example, using the formalism of Grothendieck's coherators
%%   \cite{maltsiniotis2010grothendieck}, or of Batanin's globular operads
%%   \cite{batanin1998monoidal}. (In fact, each of these formalism give rise to
%%   many different possible notions of weak $\oo$\nbd{}categories, which are
%%   conjectured to be all equivalent, at least in some higher categorical
%%   sense.)
%% \end{named}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
